# VETIT #
## Einleitung ##
VETIT bietet eine moderne Plattform, die Lehrkräfte an berufsbildenden Schulen dazu benutzen können, das Lernfeldkonzept umzusetzen. 

Es bedeutet

**V**ocational **E**ducation and **T**raining for **E**lectronic **T**echnology and **I**nformation **T**echnology 

Lehrkräfte können ihre entwickelten Lernsituationen sowohl anderen Lehrkräften, als auch anderen Akteuren der beruflichen Bildung zur Verfügung stellen. 

Dies fördert insbesondere
*  die Lernortkooperation,
*  die Umsetzung des Lernfeldkonzepts und
*  die Etablierung von Handlungsorientierung

an berufsbildenden Schulen.

## ToDo ##

* [ ] Aufbau eines GitLabs
* [ ] Meldung an BIBB
* [ ] ...